tree grammar TExpr3;

options {
  tokenVocab=Expr;

  ASTLabelType=CommonTree;

  output=template;
  superClass = TreeParserTmpl;
}

@header {
package tb.antlr.kompilator;
}

@members {
  Integer level = 0;
}

prog    : (e+=expr | d+=decl)* -> program(name = { $e }, deklaracje = { $d });

decl  :
        ^(VAR i1=ID) { globals.newSymbol($ID.text); } -> declare(name = { $ID.text })
    ;
    catch [RuntimeException ex] {errorID(ex,$i1);}

expr    : ^(PLUS  e1=expr e2=expr) -> add(p1 = { $e1.st }, p2 = { $e2.st })
        | ^(MINUS e1=expr e2=expr) -> sub(p1 = { $e1.st }, p2 = { $e2.st })
        | ^(MUL   e1=expr e2=expr) -> mul(p1 = { $e1.st }, p2 = { $e2.st })
        | ^(DIV   e1=expr e2=expr) -> div(p1 = { $e1.st }, p2 = { $e2.st })
        | ^(PODST i1=ID   e2=expr) -> assign(name = { $i1.text }, value = { $e2.st })
        | INT                      -> int(val = { $INT.text })
        | ID                       -> id(name = { $ID.text })
        | ^(IF e1=expr e2=expr e3=expr?) {level++;} -> if(condition = { $e1.st }, when_true = { $e2.st }, when_false = { $e3.st }, level = { level.toString() })
    ;    